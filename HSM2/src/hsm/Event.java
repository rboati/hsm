package hsm;

import java.util.EventObject;

@SuppressWarnings("serial")
public class Event extends EventObject {

	private static final Object nullSender = new Object();

	Event() {
		super(nullSender);
	}
}
