package hsm;


@SuppressWarnings("serial")
public final class InitEvent extends Event {
	public static final InitEvent instance = new InitEvent();

	private InitEvent() {}
}