package hsm;


@SuppressWarnings("serial")
public final class ExitEvent extends Event {
	public static final ExitEvent instance = new ExitEvent();

	private ExitEvent() {}
}