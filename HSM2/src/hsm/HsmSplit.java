package hsm;

public class HsmSplit<H extends Hsm> {
	
	protected final H hsm;

	protected interface State extends Hsm.State {}

	public HsmSplit(H hsm) {
		this.hsm = hsm;
	}

	protected final Hsm.State.Return tran(Hsm.State state) {
		return this.hsm.tran(state);
	}

    protected final Hsm.State.Return parent() {
        return this.hsm.parent();
    }

	protected final Hsm.State.Return parent(Hsm.State state) {
		return this.hsm.parent(state);
	}

	protected final Hsm.State.Return handled() {
		return this.hsm.handled();
	}

}
