package hsm;

import java.util.EventListener;
import java.util.EventObject;


public class Hsm implements EventListener {

	protected static interface State {
		static final class Return {
			private Return() {}
		}

		Return execute(final EventObject e);
	}
	
	protected final State.Return tran(State state) {
		this.mState = state;
		return RET_TRAN;
	} 

	protected final State.Return parent() {
		this.mState = TOP_STATE;
		return RET_PARENT;
	} 

	protected final State.Return parent(State state) {
		this.mState = state;
		return RET_PARENT;
	} 
	
	protected final State.Return handled() {
		return RET_HANDLED;
	}

	public boolean isIn(State state) {
	    State s = mState;
	    State.Return r;
	    boolean inState = false;
	    do {
	        if (mState == state) {
	            inState = true;
	            r = RET_IGNORED;
	        } else {
	            r = mState.execute(EmptyEvent.instance);
	        }
	    } while (r != RET_IGNORED);
	    mState = s;
	    return inState;
	}

	public void init(State initialState) {
		this.init(initialState, InitEvent.instance);
	}
	
	public final void init(State initialState, final Event e) {
		mState = initialState;

		if (mState.execute(e) != RET_TRAN)
			throw new RuntimeException();

		State t = TOP_STATE;
		do {		
			int ip = 0;
			mPath[0] = mState;
			mState.execute(EmptyEvent.instance);
			while (mState != t) {
				++ip;
				mPath[ip] = mState;
				mState.execute(EmptyEvent.instance);
			}
			
			mState = mPath[0];

			assert ip < MAX_NEST_DEPTH;

			do {
				mPath[ip].execute(EntryEvent.instance);
				--ip;
			} while (ip >= 0);

			t = mPath[0];
		} while (t.execute(InitEvent.instance) == RET_TRAN);
		mState = t;
	}

	public final void receive(final EventObject e) {
		State s;
		State t;
		State.Return r;

		t = mState;

		do {
			s = mState;
			r = s.execute(e);
		} while (r == RET_PARENT);

		if (r == RET_TRAN) {
			int ip = -1;
			int iq;

			mPath[0] = mState;
			mPath[1] = t;
			while (t != s) {
				if (t.execute(ExitEvent.instance) == RET_HANDLED) {
					t.execute(EmptyEvent.instance);
				}
				t = mState;
			}
			t = mPath[0];

			if (s == t) {
				s.execute(ExitEvent.instance);
				ip = 0;
			} else {
				t.execute(EmptyEvent.instance);
				t = mState;
				if (s == t) {
					ip = 0;
				} else {
					s.execute(EmptyEvent.instance);
					if (mState == t) {
						s.execute(ExitEvent.instance);
						ip = 0;
					} else {
						if (mState == mPath[0]) {
							s.execute(ExitEvent.instance);
						} else {
							iq = 0;
							ip = 1;
							mPath[1] = t;
							t = mState;
							r = mPath[1].execute(EmptyEvent.instance);
							while (r == RET_PARENT) {
								++ip;
								mPath[ip] = mState;
								if (mState == s) {
									iq = 1;
									assert ip < MAX_NEST_DEPTH;
									--ip;
									r = RET_HANDLED;
								} else {
									r = mState.execute(EmptyEvent.instance);
								}
							}
							if (iq == 0) {
								assert ip < MAX_NEST_DEPTH;
								s.execute(ExitEvent.instance);
								iq = ip;
								r = RET_IGNORED;
								do {
									if (t == mPath[iq]) {
										r = RET_HANDLED;
										ip = iq - 1;
										iq = -1;
									} else {
										--iq;
									}
								} while (iq >= 0);

								if (r != RET_HANDLED) {
									r = RET_IGNORED;
									do {
										if (t.execute(ExitEvent.instance) == RET_HANDLED) {
											t.execute(EmptyEvent.instance);
										}
										t = mState;
										iq = ip;
										do {
											if (t == mPath[iq]) {
												ip = iq - 1;
												iq = -1;
												r = RET_HANDLED;
											} else {
												--iq;
											}
										} while (iq >= 0);
									} while (r != RET_HANDLED);
								}
							}
						}
					}
				}
			}

			for (; ip >= 0; --ip) {
				mPath[ip].execute(EntryEvent.instance);
			}
			t = mPath[0];
			mState = t;

			while (t.execute(InitEvent.instance) == RET_TRAN) {
				ip = 0;
				mPath[0] = mState;
				mState.execute(EmptyEvent.instance);
				while (mState != t) {
					++ip;
					mPath[ip] = mState;
					mState.execute(EmptyEvent.instance);
				}
				mState = mPath[0];

				assert ip < MAX_NEST_DEPTH;

				do {
					mPath[ip].execute(EntryEvent.instance);
					--ip;
				} while (ip >= 0);

				t = mPath[0];
			}
		}
		mState = t;
	}
	
	
	private State mState = null;
	private final State[] mPath = new State[MAX_NEST_DEPTH];

    private static final int MAX_NEST_DEPTH = 8;

    private static final State.Return RET_HANDLED = new State.Return();
	private static final State.Return RET_IGNORED = new State.Return();
	private static final State.Return RET_TRAN = new State.Return();
	private static final State.Return RET_PARENT = new State.Return();

	@SuppressWarnings("serial")
	private static final class EmptyEvent extends Event {
		private static final EmptyEvent instance = new EmptyEvent();
		private EmptyEvent() {}
	}

    private static final State TOP_STATE = new State() {
        public State.Return execute(EventObject e) {
            return RET_IGNORED;
        }
    };


}
