package test;

import hsm.EntryEvent;
import hsm.ExitEvent;
import hsm.Hsm;
import hsm.InitEvent;

import java.util.EventObject;



public class HsmExample extends Hsm {

	int mFoo;
	
	@SuppressWarnings("serial")
	static class Event extends EventObject {
		private static final Object nullSender = new Object();
		Event() {
			super(nullSender);
		}
		
		public String toString() { return this.getClass().getSimpleName().substring(0, 1); }
	}
	
	@SuppressWarnings("serial")
	static final class A_Event extends Event {}
	@SuppressWarnings("serial")
	static final class B_Event extends Event {}
	@SuppressWarnings("serial")
	static final class C_Event extends Event {}
	@SuppressWarnings("serial")
	static final class D_Event extends Event {}
	@SuppressWarnings("serial")
	static final class E_Event extends Event {}
	@SuppressWarnings("serial")
	static final class F_Event extends Event {}
	@SuppressWarnings("serial")
	static final class G_Event extends Event {}
	@SuppressWarnings("serial")
	static final class H_Event extends Event {}
	@SuppressWarnings("serial")
	static final class I_Event extends Event {}

	State initial = new State() {
		public State.Return execute(EventObject e) {
			print("TOP_STATE-INIT;");
			mFoo = 0;
			return tran(s2);
		}
	};

	State s = new State() {
		public State.Return execute(EventObject e) {
			do {
				if (e instanceof EntryEvent) {
					print("s-ENTRY;");
					return handled();
				}

				if (e instanceof ExitEvent) {
					print("s-EXIT;");
					return handled();
				}

				if (e instanceof InitEvent) {
					print("s-INIT;");
					return tran(s11);
				}

				if (e instanceof E_Event) {
					print("s-E;");
					return tran(s11);
				}

				if (e instanceof I_Event) {
					if (mFoo != 0) {
						print("s-I;");
						mFoo = 0;
						return handled();
					}
					break;
				}
				
			} while (false);

			return parent();
		}
	};

	State s1 = new State() {
		public State.Return execute(EventObject e) {
			
			if (e instanceof EntryEvent) {
				print("s1-ENTRY;");
				return handled();	
			}
			
			if (e instanceof ExitEvent) {
				print("s1-EXIT;");
				return handled();
			}
			
			if (e instanceof InitEvent) {
				print("s1-INIT;");
				return tran(s11);	
			}
			
			if (e instanceof A_Event) {
				print("s1-A;");
				return tran(s1);	
			}
			
			if (e instanceof B_Event) {
				print("s1-B;");
				return tran(s11);
			}
			
			if (e instanceof C_Event) {
				print("s1-C;");
				return tran(s2);	
			}
			
			if (e instanceof D_Event) { 
				return choice1(e);
			}
			
			if (e instanceof F_Event) {
				print("s1-F;");
				return tran(s211);	
			}
			
			if (e instanceof I_Event) {
				print("s1-I;");
				return handled();
			}

			return parent(s);
		}

	};

	State s11 = new State() {
		public State.Return execute(EventObject e) {
			do {
				if (e instanceof EntryEvent) {
					print("s11-ENTRY;");
					return handled();
				}

				if (e instanceof ExitEvent) {
					print("s11-EXIT;");
					return handled();
				}

				if (e instanceof D_Event) {
					if (mFoo != 0) {
						print("s11-D;");
						mFoo = 0;
						return tran(s1);
					}
					break;
				}

				if (e instanceof G_Event) {
					print("s11-G;");
					return tran(s211);
				}

				if (e instanceof H_Event) {
					print("s11-H;");
					return tran(s);
				}
				
			} while (false);

			return parent(s1);
		}
	};

	State s2 = new State() {
		public State.Return execute(EventObject e) {
			
			do {
				if (e instanceof EntryEvent) {
					print("s2-ENTRY;");
					return handled();
				}

				if (e instanceof ExitEvent) {
					print("s2-EXIT;");
					return handled();
				}

				if (e instanceof InitEvent) {
					print("s2-INIT;");
					return tran(s211);
				}

				if (e instanceof C_Event) {
					print("s2-C;");
					return tran(s1);
				}

				if (e instanceof F_Event) {
					print("s2-F;");
					return tran(s11);
				}

				if (e instanceof I_Event) {
					if (mFoo == 0) {
						print("s2-I;");
						mFoo = 1;
						return handled();
					}
					break;
				}
				
			} while (false);
			
			return parent(s);
		}
	};

	State s21 = new State() {
		public State.Return execute(EventObject e) {
			if (e instanceof EntryEvent) {
				print("s21-ENTRY;");
				return handled();
			}
			
			if (e instanceof ExitEvent) {
				print("s21-EXIT;");
				return handled();
			}
			
			if (e instanceof InitEvent) {
				print("s21-INIT;");
				return tran(s211);
			}
			
			if (e instanceof A_Event) {
				print("s21-A;");
				return tran(s21);
			}
			
			if (e instanceof B_Event) {
				print("s21-B;");
				return tran(s211);
			}
			
			if (e instanceof G_Event) {
				print("s21-G;");
				return tran(s1);
			}
			
			return parent(s2);
		}
	};

	State s211 = new State() {
		public State.Return execute(EventObject e) {

			if (e instanceof EntryEvent) {
				print("s211-ENTRY;");
				return handled();
			}

			if (e instanceof ExitEvent) {
				print("s211-EXIT;");
				return handled();
			}

			if (e instanceof D_Event) {
				print("s211-D;");
				return tran(s21);
			}

			if (e instanceof H_Event) {
				print("s211-H;");
				return tran(s);
			}

			return parent(s21);
		}

	};

	
	StringBuilder trace = new StringBuilder();

	void print(String s) {
		System.out.print(s);
		trace.append(s);
	}

	State.Return choice1(final EventObject e) {
		if (mFoo == 0) {
			print("s1-D;");
			mFoo = 1;
			return tran(s);
		}
		return handled();
	}
	

	void send(Event e) {
		print("\n" + e.toString() + ":");
		receive(e);
	}


	public static void main(String[] args) {

		HsmExample fsm = new HsmExample();

		fsm.init(fsm.initial);
		
		fsm.send(new A_Event());
		fsm.send(new B_Event());
		fsm.send(new D_Event());
		fsm.send(new E_Event());
		fsm.send(new I_Event());
		fsm.send(new F_Event());
		fsm.send(new I_Event());
		fsm.send(new I_Event());
		fsm.send(new F_Event());
		fsm.send(new A_Event());
		fsm.send(new B_Event());
		fsm.send(new D_Event());
		fsm.send(new D_Event());
		fsm.send(new E_Event());
		fsm.send(new G_Event());
		fsm.send(new H_Event());
		fsm.send(new H_Event());
		fsm.send(new C_Event());
		fsm.send(new G_Event());
		fsm.send(new C_Event());
		fsm.send(new C_Event());
	}
}
