package test;

import hsm.*;

import java.util.EventObject;

import test.HsmExampleSplit1.*;




public class HsmExampleSplit2 extends HsmSplit<HsmExampleSplit1> {
	
	HsmExampleSplit2(HsmExampleSplit1 h) {
		super(h);
	}

	final State s211 = new State() {
		public Return execute(EventObject e) {

			if (e instanceof EntryEvent) {
				hsm.print("s211-ENTRY;");
				return handled();
			}

			if (e instanceof ExitEvent) {
				hsm.print("s211-EXIT;");
				return handled();
			}

			if (e instanceof D_Event) {
				hsm.print("s211-D;");
				return tran(hsm.s21);
			}

			if (e instanceof H_Event) {
				hsm.print("s211-H;");
				return tran(hsm.s);
			}

			return parent(hsm.s21);
		}

	};
}
