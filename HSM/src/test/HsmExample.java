package test;

import hsm.Event;
import hsm.Hsm;

public class HsmExample extends Hsm {

	int mFoo;

	static final int A_SIG = 0;
	static final int B_SIG = 1;
	static final int C_SIG = 2;
	static final int D_SIG = 3;
	static final int E_SIG = 4;
	static final int F_SIG = 5;
	static final int G_SIG = 6;
	static final int H_SIG = 7;
	static final int I_SIG = 8;

	State initial = new State() {
		public State.Return execute(Event e) {
			print("topState-INIT;");
			mFoo = 0;
			return tran(s2);
		}
	};

	State s = new State() {
		public State.Return execute(Event e) {
			switch (e.signal) {

			case ENTRY_SIG:
				print("s-ENTRY;");
				return handled();

			case EXIT_SIG:
				print("s-EXIT;");
				return handled();

			case INIT_SIG:
				print("s-INIT;");
				return tran(s11);

			case E_SIG:
				print("s-E;");
				return tran(s11);

			case I_SIG:
				if (mFoo != 0) {
					print("s-I;");
					mFoo = 0;
					return handled();
				}
				break;

			}
			return parent();
		}

	};

	State s1 = new State() {
		public State.Return execute(Event e) {
			switch (e.signal) {

			case ENTRY_SIG:
				print("s1-ENTRY;");
				return handled();

			case EXIT_SIG:
				print("s1-EXIT;");
				return handled();

			case INIT_SIG:
				print("s1-INIT;");
				return tran(s11);

			case A_SIG:
				print("s1-A;");
				return tran(s1);

			case B_SIG:
				print("s1-B;");
				return tran(s11);

			case C_SIG:
				print("s1-C;");
				return tran(s2);

			case D_SIG:
				return choice1(e);

			case F_SIG:
				print("s1-F;");
				return tran(s211);

			case I_SIG:
				print("s1-I;");
				return handled();
			}

			return parent(s);
		}

	};

	State s11 = new State() {
		public State.Return execute(Event e) {
			switch (e.signal) {

			case ENTRY_SIG:
				print("s11-ENTRY;");
				return handled();

			case EXIT_SIG:
				print("s11-EXIT;");
				return handled();

			case D_SIG:
				if (mFoo != 0) {
					print("s11-D;");
					mFoo = 0;
					return tran(s1);
				}
				break;

			case G_SIG:
				print("s11-G;");
				return tran(s211);

			case H_SIG:
				print("s11-H;");
				return tran(s);
			}

			return parent(s1);
		}
	};

	State s2 = new State() {
		public State.Return execute(Event e) {
			switch (e.signal) {

			case ENTRY_SIG:
				print("s2-ENTRY;");
				return handled();

			case EXIT_SIG:
				print("s2-EXIT;");
				return handled();

			case INIT_SIG:
				print("s2-INIT;");
				return tran(s211);

			case C_SIG:
				print("s2-C;");
				return tran(s1);

			case F_SIG:
				print("s2-F;");
				return tran(s11);

			case I_SIG:
				if (mFoo == 0) {
					print("s2-I;");
					mFoo = 1;
					return handled();
				}
				break;

			}
			return parent(s);
		}
	};

	State s21 = new State() {
		public State.Return execute(Event e) {
			switch (e.signal) {

			case ENTRY_SIG:
				print("s21-ENTRY;");
				return handled();

			case EXIT_SIG:
				print("s21-EXIT;");
				return handled();

			case INIT_SIG:
				print("s21-INIT;");
				return tran(s211);

			case A_SIG:
				print("s21-A;");
				return tran(s21);

			case B_SIG:
				print("s21-B;");
				return tran(s211);

			case G_SIG:
				print("s21-G;");
				return tran(s1);
			}
			return parent(s2);
		}
	};

	State s211 = new State() {
		public State.Return execute(Event e) {
			switch (e.signal) {

			case ENTRY_SIG:
				print("s211-ENTRY;");
				return handled();

			case EXIT_SIG:
				print("s211-EXIT;");
				return handled();

			case D_SIG:
				print("s211-D;");
				return tran(s21);

			case H_SIG:
				print("s211-H;");
				return tran(s);
			}
			return parent(s21);
		}

	};

	StringBuilder trace = new StringBuilder();

	void print(String s) {
		System.out.print(s);
		trace.append(s);
	}

	State.Return choice1(final Event e) {
		if (mFoo == 0) {
			print("s1-D;");
			mFoo = 1;
			return tran(s);
		}
		return handled();
	}
	

	void send(int signal) {
		char c = (char) ('A' + signal);
		print("\n" + Character.toString(c) + ":");
		dispatch(new Event(signal));
	}


	public static void main(String[] args) {

		HsmExample fsm = new HsmExample();

		fsm.init(fsm.initial);
		
		fsm.send(HsmExample.A_SIG);
		fsm.send(HsmExample.B_SIG);
		fsm.send(HsmExample.D_SIG);
		fsm.send(HsmExample.E_SIG);
		fsm.send(HsmExample.I_SIG);
		fsm.send(HsmExample.F_SIG);
		fsm.send(HsmExample.I_SIG);
		fsm.send(HsmExample.I_SIG);
		fsm.send(HsmExample.F_SIG);
		fsm.send(HsmExample.A_SIG);
		fsm.send(HsmExample.B_SIG);
		fsm.send(HsmExample.D_SIG);
		fsm.send(HsmExample.D_SIG);
		fsm.send(HsmExample.E_SIG);
		fsm.send(HsmExample.G_SIG);
		fsm.send(HsmExample.H_SIG);
		fsm.send(HsmExample.H_SIG);
		fsm.send(HsmExample.C_SIG);
		fsm.send(HsmExample.G_SIG);
		fsm.send(HsmExample.C_SIG);
		fsm.send(HsmExample.C_SIG);
	}
}
