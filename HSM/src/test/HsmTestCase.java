package test;

import static org.junit.Assert.*;

import org.junit.Test;

public class HsmTestCase {

	@Test
	public void example() {

		final String expected = "topState-INIT;s-ENTRY;s2-ENTRY;s2-INIT;s21-ENTRY;s211-ENTRY;\n"
		+ "A:s21-A;s211-EXIT;s21-EXIT;s21-ENTRY;s21-INIT;s211-ENTRY;\n"
		+ "B:s21-B;s211-EXIT;s211-ENTRY;\n"
		+ "D:s211-D;s211-EXIT;s21-INIT;s211-ENTRY;\n"
		+ "E:s-E;s211-EXIT;s21-EXIT;s2-EXIT;s1-ENTRY;s11-ENTRY;\n"
		+ "I:s1-I;\n"
		+ "F:s1-F;s11-EXIT;s1-EXIT;s2-ENTRY;s21-ENTRY;s211-ENTRY;\n"
		+ "I:s2-I;\n"
		+ "I:s-I;\n"
		+ "F:s2-F;s211-EXIT;s21-EXIT;s2-EXIT;s1-ENTRY;s11-ENTRY;\n"
		+ "A:s1-A;s11-EXIT;s1-EXIT;s1-ENTRY;s1-INIT;s11-ENTRY;\n"
		+ "B:s1-B;s11-EXIT;s11-ENTRY;\n"
		+ "D:s1-D;s11-EXIT;s1-EXIT;s-INIT;s1-ENTRY;s11-ENTRY;\n"
		+ "D:s11-D;s11-EXIT;s1-INIT;s11-ENTRY;\n"
		+ "E:s-E;s11-EXIT;s1-EXIT;s1-ENTRY;s11-ENTRY;\n"
		+ "G:s11-G;s11-EXIT;s1-EXIT;s2-ENTRY;s21-ENTRY;s211-ENTRY;\n"
		+ "H:s211-H;s211-EXIT;s21-EXIT;s2-EXIT;s-INIT;s1-ENTRY;s11-ENTRY;\n"
		+ "H:s11-H;s11-EXIT;s1-EXIT;s-INIT;s1-ENTRY;s11-ENTRY;\n"
		+ "C:s1-C;s11-EXIT;s1-EXIT;s2-ENTRY;s2-INIT;s21-ENTRY;s211-ENTRY;\n"
		+ "G:s21-G;s211-EXIT;s21-EXIT;s2-EXIT;s1-ENTRY;s1-INIT;s11-ENTRY;\n"
		+ "C:s1-C;s11-EXIT;s1-EXIT;s2-ENTRY;s2-INIT;s21-ENTRY;s211-ENTRY;\n"
		+ "C:s2-C;s211-EXIT;s21-EXIT;s2-EXIT;s1-ENTRY;s1-INIT;s11-ENTRY;";
		
		HsmExample fsm = new HsmExample();

		fsm.init(fsm.initial);

		fsm.send(HsmExample.A_SIG);
		fsm.send(HsmExample.B_SIG);
		fsm.send(HsmExample.D_SIG);
		fsm.send(HsmExample.E_SIG);
		fsm.send(HsmExample.I_SIG);
		fsm.send(HsmExample.F_SIG);
		fsm.send(HsmExample.I_SIG);
		fsm.send(HsmExample.I_SIG);
		fsm.send(HsmExample.F_SIG);
		fsm.send(HsmExample.A_SIG);
		fsm.send(HsmExample.B_SIG);
		fsm.send(HsmExample.D_SIG);
		fsm.send(HsmExample.D_SIG);
		fsm.send(HsmExample.E_SIG);
		fsm.send(HsmExample.G_SIG);
		fsm.send(HsmExample.H_SIG);
		fsm.send(HsmExample.H_SIG);
		fsm.send(HsmExample.C_SIG);
		fsm.send(HsmExample.G_SIG);
		fsm.send(HsmExample.C_SIG);
		fsm.send(HsmExample.C_SIG);

		assertEquals(expected, fsm.trace.toString());

	}

}
