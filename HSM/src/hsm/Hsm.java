package hsm;

public class Hsm {
	
	protected static interface State {
        public static final class Return {
			private Return() {}
		}

		Return execute(final Event e);
	}

    protected static final int INIT_SIG = -2;
    protected static final int ENTRY_SIG = -3;
	protected static final int EXIT_SIG = -4;

	protected final State.Return tran(State state) {
		this.mState = state;
		return RET_TRAN;
	}

    protected final State.Return parent() {
        this.mState = TOP_STATE;
        return RET_PARENT;
    }

    protected final State.Return parent(State state) {
		this.mState = state;
		return RET_PARENT;
	} 
	
	protected final State.Return handled() {
		return RET_HANDLED;
	}
	

	public boolean isIn(State state) {
	    State s = mState;
	    State.Return r;
	    boolean inState = false;
	    do {
	        if (mState == state) {
	            inState = true;
	            r = RET_IGNORED;
	        } else {
	            r = mState.execute(EMPTY_EVENT);
	        }
	    } while (r != RET_IGNORED);
	    mState = s;
	    return inState;
	}

	public void init(State initialState) {
		this.init(initialState, INIT_EVENT);
	}
	
	public final void init(State initialState, final Event e) {
		mState = initialState;

		if (mState.execute(e) != RET_TRAN)
			throw new RuntimeException();

		State t = TOP_STATE;
		do {		
			int ip = 0;
			mPath[0] = mState;
			mState.execute(EMPTY_EVENT);
			while (mState != t) {
				++ip;
				mPath[ip] = mState;
				mState.execute(EMPTY_EVENT);
			}
			
			mState = mPath[0];

			assert ip < MAX_NEST_DEPTH;

			do {
				mPath[ip].execute(ENTRY_EVENT);
				--ip;
			} while (ip >= 0);

			t = mPath[0];
		} while (t.execute(INIT_EVENT) == RET_TRAN);
		mState = t;
	}

	public final void dispatch(final Event e) {
		State s;
		State t;
		State.Return r;

		t = mState;

		do {
			s = mState;
			r = s.execute(e);
		} while (r == RET_PARENT);

		if (r == RET_TRAN) {
			int ip = -1;
			int iq;

			mPath[0] = mState;
			mPath[1] = t;
			while (t != s) {
				if (t.execute(EXIT_EVENT) == RET_HANDLED) {
					t.execute(EMPTY_EVENT);
				}
				t = mState;
			}
			t = mPath[0];

			if (s == t) {
				s.execute(EXIT_EVENT);
				ip = 0;
			} else {
				t.execute(EMPTY_EVENT);
				t = mState;
				if (s == t) {
					ip = 0;
				} else {
					s.execute(EMPTY_EVENT);
					if (mState == t) {
						s.execute(EXIT_EVENT);
						ip = 0;
					} else {
						if (mState == mPath[0]) {
							s.execute(EXIT_EVENT);
						} else {
							iq = 0;
							ip = 1;
							mPath[1] = t;
							t = mState;
							r = mPath[1].execute(EMPTY_EVENT);
							while (r == RET_PARENT) {
								++ip;
								mPath[ip] = mState;
								if (mState == s) {
									iq = 1;
									assert ip < MAX_NEST_DEPTH;
									--ip;
									r = RET_HANDLED;
								} else {
									r = mState.execute(EMPTY_EVENT);
								}
							}
							if (iq == 0) {
								assert ip < MAX_NEST_DEPTH;
								s.execute(EXIT_EVENT);
								iq = ip;
								r = RET_IGNORED;
								do {
									if (t == mPath[iq]) {
										r = RET_HANDLED;
										ip = iq - 1;
										iq = -1;
									} else {
										--iq;
									}
								} while (iq >= 0);

								if (r != RET_HANDLED) {
									r = RET_IGNORED;
									do {
										if (t.execute(EXIT_EVENT) == RET_HANDLED) {
											t.execute(EMPTY_EVENT);
										}
										t = mState;
										iq = ip;
										do {
											if (t == mPath[iq]) {
												ip = iq - 1;
												iq = -1;
												r = RET_HANDLED;
											} else {
												--iq;
											}
										} while (iq >= 0);
									} while (r != RET_HANDLED);
								}
							}
						}
					}
				}
			}

			for (; ip >= 0; --ip) {
				mPath[ip].execute(ENTRY_EVENT);
			}
			t = mPath[0];
			mState = t;

			while (t.execute(INIT_EVENT) == RET_TRAN) {
				ip = 0;
				mPath[0] = mState;
				mState.execute(EMPTY_EVENT);
				while (mState != t) {
					++ip;
					mPath[ip] = mState;
					mState.execute(EMPTY_EVENT);
				}
				mState = mPath[0];

				assert ip < MAX_NEST_DEPTH;

				do {
					mPath[ip].execute(ENTRY_EVENT);
					--ip;
				} while (ip >= 0);

				t = mPath[0];
			}
		}
		mState = t;
	}
	
	
	private State mState;
	private State[] mPath = new State[MAX_NEST_DEPTH];
	
	private static final int MAX_NEST_DEPTH = 8;

	private static final int EMPTY_SIG = -1;
	
	private static final Event EMPTY_EVENT = new Event(EMPTY_SIG);
	private static final Event INIT_EVENT = new Event(INIT_SIG);
	private static final Event ENTRY_EVENT = new Event(ENTRY_SIG);
	private static final Event EXIT_EVENT = new Event(EXIT_SIG);

    private static final State.Return RET_PARENT = new State.Return();
    private static final State.Return RET_TRAN = new State.Return();
    private static final State.Return RET_IGNORED = new State.Return();
    private static final State.Return RET_HANDLED = new State.Return();

    private static final State TOP_STATE = new State() {
        public State.Return execute(Event e) {
            return RET_IGNORED;
        }
    };



}
