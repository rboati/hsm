package hsm;

public class HsmSplit<H extends Hsm> {
	
	protected final H hsm;

    protected static final int INIT_SIG = Hsm.INIT_SIG;
    protected static final int ENTRY_SIG = Hsm.ENTRY_SIG;
    protected static final int EXIT_SIG = Hsm.EXIT_SIG;


    protected interface State extends Hsm.State {}

	public HsmSplit(H hsm) {
		this.hsm = hsm;
	}

	protected final Hsm.State.Return tran(Hsm.State state) {
		return this.hsm.tran(state);
	}

    protected final Hsm.State.Return parent() {
        return this.hsm.parent();
    }

    protected final Hsm.State.Return parent(Hsm.State state) {
		return this.hsm.parent(state);
	}

	protected final Hsm.State.Return handled() {
		return this.hsm.handled();
	}

}
